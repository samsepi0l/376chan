const http = require("http");
const fs = require("fs");
const qs = require("querystring");
const json = require("./data/accounts.json");

const server = http.createServer((req, res) => {
	if (req.url === "/styles/styles.css") {
		res.statusCode = 200;
		res.setHeader("Content-Type", "text/css");
		fs.readFile("styles/styles.css", function(error, data) {
			if (error) {
				res.writeHead(404);
				res.write("Error: File not found.");
			} else {
				res.write(data);
			}

			res.end();
		});
	}

	if (req.url === "/") {
		res.statusCode = 200;
		res.setHeader("Content-Type", "text/html");
		fs.readFile("./pages/home.html", function(error, data) {
			if (error) {
				res.writeHead(404);
				res.write("Error: File not found.");
			} else {
				res.write(data);
			}

			res.end();
		});
	}

	if (req.url === "/about") {
		res.statusCode = 200;
		res.setHeader("Content-Type", "text/html");
		fs.readFile("./pages/about.html", function(error, data) {
			if (error) {
				res.writeHead(404);
				res.write("Error: File not found.");
			} else {
				res.write(data);
			}

			res.end();
		});
	}

	if (req.method === "POST" && req.url === "/members") {
		res.statusCode = 200;
		res.setHeader("Content-Type", "text/json");
		var body = "";
		var i = 1;

		req.on("data", function(data) {
			body = body + data;
		});

		req.on("end", function() {
			var post = qs.parse(body);
			if (post.username === json[0].username && post.password === json[0].password) {
				res.writeHead(301, { Location: "/" + post.username });
			} else {
				res.writeHead(301, { Location: "/wrongInfo" });
			}
			res.end();
		});

	}

	if (req.url === "/sweatshirt") {
		res.statusCode = 200;
		res.setHeader("Content-Type", "text/html");
		fs.readFile("./pages/sweatshirt.html", function(error, data) {
			if (error) {
				res.writeHead(404);
				res.write("Error: File not found.");
			} else {
				res.write(data);
			}

			res.end();
		});
	}

	if (req.url === "/wrongInfo") {
		res.statusCode = 200;
		res.setHeader("Content-Type", "text/html");
		fs.readFile("./pages/wrongInfo.html", function(error, data) {
			if (error) {
				res.writeHead(404);
				res.write("Error: File not found.");
			} else {
				res.write(data);
			}

			res.end();
		});
	}

	if (req.url === "/contact") {
		res.statusCode = 200;
		res.setHeader("Content-Type", "text/html");
		fs.readFile("./pages/contact.html", function(error, data) {
			if (error) {
				res.writeHead(404);
				res.write("Error: File not found.");
			} else {
				res.write(data);
			}

			res.end();
		});
	}

});

const port = process.env.port || 8080;

server.listen(port, () => console.log(`Server running on port ${port}`));
